package src;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Solution {
	//based on the maximum value a signed in can have in java;
	static final int INFINITY = 2147483647;
	int m, n, e;
	int[][] grid;
	// uses one integer value for a coordinate (key = mcoord*n +ncoord)
	Map<Integer, Integer> time; 
	Set<Integer> unvisited = new HashSet<Integer>();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Solution s = new Solution();
		s.readStdIn();
		s.runAlgorithm();
	}
	/**
	 * reads in the input from the stdIn in the format:
	 * line 1: 			m n (where m and n are the dimensions of the grid)
	 * next m lines: 	(n integers corresponding to the values at each part of the grid)
	 * next line: 		e (number of starting locations coming up to  run the minimum path algorithm on)
	 * next e lines: 	2 integers representing the starting coordinate to run the minimum path algorithm on
	 */
	public void readStdIn() {
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(
				System.in));
		String line;
		String[] inputSplit;
		// create map for djikstras
		time = new HashMap<Integer, Integer>(m * n);
		try {
			line = stdIn.readLine();
			inputSplit = line.split(" ");
			// check to ensure input is in the correct format
			if (inputSplit.length == 2) {
				m = Integer.parseInt(inputSplit[0].replaceAll("\\s", ""));
				n = Integer.parseInt(inputSplit[1].replaceAll("\\s", ""));
			} else {
				System.out.println("Program exiting, incorrect input format.");
				System.exit(0);
			}
			grid = new int[m][n];
			// populate the grid
			for (int i = 0; i < m; ++i) {
				line = stdIn.readLine();
				inputSplit = line.split(" ", n);
				for (int j = 0; j < n; ++j) {
					int coord = i * n + j;
					// populate the grid
					grid[i][j] = Integer.parseInt(inputSplit[j].replaceAll(
							"\\s", ""));
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void runAlgorithm() {
		String line;
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(
				System.in));
		String[] inputSplit;
		int startM;
		int startN;
		// read the number of employees;
		try {
			line = stdIn.readLine();
			e = Integer.parseInt(line.replaceAll("\\s", ""));

			for (int i = 0; i < e; ++i) {
				line = stdIn.readLine();
				inputSplit = line.split(" ", 2);
				startM = Integer.parseInt(inputSplit[0].replaceAll("\\s", ""));
				startN = Integer.parseInt(inputSplit[1].replaceAll("\\s", ""));
				//greedy(startM, startN);
				dijkstras(startM, startN);
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	// optimal implementation adapting Djikstra's algorithm
	public void dijkstras(int startM, int startN) {

		if (startM >= m || startN >= n || startM < 0 || startN < 0) {
			System.out
					.println("Employee can't start out of the bounds of the grid");
		} else {
			// populate the unvisited set and map
			for (int i = 0; i < m; ++i) {
				for (int j = 0; j < n; ++j) {
					int coord = i * n + j;
					// set the current time values to INFINITY
					time.put(coord, INFINITY);
					// add the coordinate to the set of unvisited nodes
					unvisited.add(coord);
				}
			}
			int currM = startM;
			int currN = startN;
			int currPos = currM * n + currN;
			// set the starting position to be only equal to 
			// the amount of time to wait at that light
			time.put(currPos, grid[currM][currN]); 
			while (!unvisited.isEmpty()) {
				currPos = getMin();
				unvisited.remove(currPos);
				currM = currPos/n;
				currN= currPos%n;
				int alt;
				// check both potential neighbours of the current node to see 
				// if there is a better path to them than what already exists
				if(isNotVisited(currPos+1)){
					//neighbour to the right has not been visited yet
					if(currN!=n-1){
						alt = time.get(currPos) + grid[currM][currN+1];
						if(alt < time.get(currPos+1)){
							//update the time taken to reach the node since the
							// path through the current node is faster/shorter
							time.put(currPos+1, alt);
						}	
					}
				}
				if(isNotVisited(currPos+n)){
					//neighbour below has not been visited yet
					if(currM!=m-1){
						alt = time.get(currPos) + grid[currM+1][currN];
						if(alt < time.get(currPos+n)){
							//update the time taken to reach the node since the
							// path through the current node is faster/shorter
							time.put(currPos+n, alt);
						}
						
					}
				}
			}
			int dest = (m-1)*n + n-1;
			System.out.println(time.get(dest));
			// System.out.println("Djikstra's: "+ time.get(dest));
			/*for(int mi = 0;mi<m;mi++){
				for (int ni = 0; ni <n; ni++){
					System.out.print(time.get(mi*n +ni) + " ");
				}
				System.out.println();
			} */
		}
	}

	/**
	 * 
	 * @return the key to the minimum value in the map that has not been visited yet
	 */
	public int getMin() {
		int min = 0;
		int minKey = 0;
		int i = 0;

		for (int key : unvisited) {
			//set the min to the value of the key on the first iteration
			if (i == 0) {
				min = time.get(key);
				minKey = key;
				i++;
			}
			if (time.get(key) < min) {
				minKey = key;
				min = time.get(key);
			}
		}

		return minKey;
	}

	/**
	 * checks to see that the node corresponding to the key has not been visited
	 * 
	 * @param key
	 *            the key to check
	 * @return true if it has not yet been visited, otherwise false
	 */
	public boolean isNotVisited(int key) {
		return unvisited.contains(key);
	}

	/**
	 * naive implementation that wont work every time
	 * simply compares the two choices the employee can take
	 * and selects the smallest. This is not an optimal solution.
	 * @param startM starting position along the M (y) axis
	 * @param startN starting position along the N (x) axis
	 */
	public void greedy(int startM, int startN) {
		int currM = startM;
		int currN = startN;
		int sum = 0;

		if (startM >= m || startN >= n || startM < 0 || startN < 0) {
			System.out
					.println("Employee can't start out of the bounds of the grid");
		} else {

			while (!arrived(currM, currN)) {
				sum += grid[currM][currN];
				if (currM == m - 1) {
					currN++;
				} else if (currN == n - 1) {
					currM++;
				} else if (grid[currM + 1][currN] < grid[currM][currN + 1]) {
					currM++;
				} else {
					currN++;
				}

			}
			sum += grid[currM][currN];
			System.out.println("GREEDY SOLUTION: " + sum);

		}
	}

	public boolean arrived(int currM, int currN) {
		if (currM == m - 1 && currN == n - 1) {
			return true;
		}
		return false;

	}


}
